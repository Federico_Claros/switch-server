from sqlite3 import Connection
import pg8000

import primary_db as pdb
import postgres_db as posdb

""" Will unwrap json data and choose the correct operation based on the info.
"""

def call_sqlite_insert(con:Connection, body:dict, id=None)->str:
    descr = body.get("descr");
    stock = body.get("stock");
    precio_base = body.get("precio_base");
    precio_costo = body.get("precio_costo");
    if( id != None):
        result = pdb.reinsert_product(con,id,descr,stock,precio_base,precio_costo);
    else:
        result = pdb.insert_product(con,descr,stock,precio_base,precio_costo);
    return result;

def call_sqlite_update(con:Connection, body:dict)->str:
    id = body.get("id");
    descr = body.get("descr");
    stock = body.get("stock");
    precio_base = body.get("precio_base");
    precio_costo = body.get("precio_costo");
    return pdb.update_product(con,id,descr,stock,precio_base,precio_costo);

def call_sqlite_delete(con:Connection, body:dict)->str:
    id = body.get("id");
    return pdb.delete_product(con,id);

def call_sqlite_select(con:Connection, body:dict)->str:
    sql = body.get("sql");
    return pdb.select(con,sql);

def call_postgres_insert(con: pg8000.Connection, body: dict, id=None) -> str:
    descr = body.get("descr")
    stock = body.get("stock")
    precio_base = body.get("precio_base")
    precio_costo = body.get("precio_costo")
    if (id != None):
        result = posdb.reinsert_product(con, id, descr, stock, precio_base, precio_costo)
    else:
        result = posdb.insert_product(con, descr, stock, precio_base, precio_costo)
    return result

def call_postgres_update(con: pg8000.Connection, body: dict) -> str:
    id = body.get("id")
    descr = body.get("descr")
    stock = body.get("stock")
    precio_base = body.get("precio_base")
    precio_costo = body.get("precio_costo")
    return posdb.update_product(con, id, descr, stock, precio_base, precio_costo)

def call_postgres_delete(con: pg8000.Connection, body: dict) -> str:
    id = body.get("id")
    return posdb.delete_product(con, id)

def call_postgres_select(con: pg8000.Connection, body: dict) -> str:
    sql = body.get("sql")
    return posdb.select(con, sql)