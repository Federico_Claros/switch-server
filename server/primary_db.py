import sys
from sqlite3 import Connection

sys.path.append("./sqlite3_layer/src/")
import Sqlite3Handler as s3h
import JsonHandler as jh

def connect()->Connection:
    """ Connects to primary database
    :returns: A Sqlite3.Connection to primary.db
    :rtype: Connection
    """
    return s3h.db_connect('./primary.db');

def close(con:Connection)->str:
    return s3h.db_close(con);

def init_tables(con:Connection):
    create_table_1 = """ CREATE TABLE IF NOT EXISTS product(
                                id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                                descr varchar(100) NOT NULL,
                                stock integer default 0 not null,
                                precio_base double precision not null,
                                precio_costo double precision not null
                        );
                        """
    return s3h.db_init_table(con,create_table_1);

def insert_product(con:Connection, descr:str, stock:int, precio_b:float, precio_c:float) -> str:
    query = f'INSERT INTO product (descr, stock, precio_base, precio_costo) VALUES ("{descr}", {stock}, {precio_b}, {precio_c})'
    return s3h.db_cud(con,query,'insert');

def reinsert_product(con:Connection, id:int, descr:str, stock:int, precio_b:float, precio_c:float) -> str:
    query = f'INSERT INTO product (id, descr, stock, precio_base, precio_costo) VALUES ({id},"{descr}", {stock}, {precio_b}, {precio_c})'
    return s3h.db_cud(con,query,'insert');

def delete_product(con:Connection, id:int) -> str:
    query = f'DELETE FROM product WHERE id = {id}';
    return s3h.db_cud(con, query, 'delete');

def update_product(con:Connection, id:int, descr, stock, precio_b, precio_c)->str:
    rtr_str = '';
    query = '';
    if (descr is None and stock is None and precio_b is None and precio_c is None):
        rtr_str = jh.parse_error("No se especifico ningun campo a modificar!");
    elif (descr != None and stock is None and precio_b is None and precio_c is None):
        query = f'UPDATE FROM product SET descr = "{descr}" WHERE id = {id}';
    elif (descr != None and stock != None and precio_b is None and precio_c is None):
        query = f'UPDATE FROM product SET descr = "{descr}", stock = {stock} WHERE id = {id}';
    elif (descr != None and stock != None and precio_b != None and precio_c is None):
        query = f'UPDATE FROM product SET descr = "{descr}", stock = {stock}, precio_base = {precio_b} WHERE id = {id}';
    else:
        query = f'UPDATE FROM product SET descr = "{descr}", stock = {stock}, precio_base = {precio_b}, precio_costo = {precio_c} WHERE id = {id}';
    if (query != ''):
        rtr_str = s3h.db_cud(con, query,'update');
    return rtr_str;

def select (con:Connection, query)->str:
    # here should be query sanitiation.
    return s3h.db_select(con,query);