import sys
from sqlite3 import Connection

sys.path.append("./sqlite3_layer/src/")
import Sqlite3Handler as s3h

""" Module that handles logger to database.
:version: 1.0
"""

def log(con:Connection, ip:str, msg:str) -> str:
    """ 
    """
    insert = f'INSERT INTO log (IP, msg) values ("{ip}", "{msg}")';
    return s3h.db_cud(con, insert, 'insert');

def init_log_table(con:Connection):
    create_table_query = """ CREATE TABLE IF NOT EXISTS log(
                                id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                                timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                                IP VARCHAR(64) NOT NULL,
                                msg VARCHAR(120)
                        );
                        """
    return s3h.db_init_table(con,create_table_query)

def connect()->Connection:
    """ Connects to logs database
    :returns: A Sqlite3.Connection to logs.db
    :rtype: Connection
    """
    return s3h.db_connect('./logs.db');

