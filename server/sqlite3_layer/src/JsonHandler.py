import json
from sqlite3 import Cursor

""" A module that handles (poorly) json serialize data in order to be send through internet.
    
    :version: 1.0
"""

def parse_msg(message:str) -> str:
    """ Parses message from database to Json dataframe as specified in json_dataframe.json
    
    :param message: Sqlite3 server's message to be parsed.
    :type message: str
    :returns: Json_string with database's message.
    :rtype: str
    """
    return json.dumps(dict(type= "message", body=message)); # dictionary is serialize into json_data
    
def parse_results(cur:Cursor) -> str:
    """ Parses executed cursor into a result set from database query (R) operation.

    :param cur: executed cursor to extract information.
    :type: Cursor
    :returns: Json_string with database's result set.
    :rtype: str
    """
    # 1º get column names from cursor and parse it into json
    l_columns = list(map(lambda x: x[0], cur.description))
    # 3º put columns and rows together into a dictionary and parse it to json.
    return json.dumps({'type':"result",'body':{'columns': l_columns, 'rows': cur.fetchall()}}, separators=(',',':'))
    
def parse_error(err:str) -> str:
    """ Parses database's error as feedback to be send.

    :param err: Database error to be parsed.
    :type: str
    :returns: Error parsed as like json_dataframe.json format.
    :rtype: str
    """
    return json.dumps(dict(type="error", body=err));

def unparse(jsn:str) -> dict:
    return json.loads(jsn);