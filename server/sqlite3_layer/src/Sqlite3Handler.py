import sqlite3
import string
import JsonHandler as jh

""" module that handles (poorly) sqlite3 connections and transactions
    
    :version: 1.0
"""

def db_connect(path:string) -> sqlite3.Connection:
    """ Sets up connection to sqlite3 database, given a path.
    :param path: The database's path were it's located.
    :type path: str
    :returns: Either a sqlite3.Connection or error string.
    :rtype: sqlite3.Connection || str
    """
    try:
        connection = sqlite3.connect(path);
    except sqlite3.Error as err:
        connection = jh.parse_error("%s"%(''.join(err.args)))
    return connection;

def db_close(con:sqlite3.Connection) -> str:
    """ Closes database connection
    :param con: The database's sqlite3.Connection object ready to be closed.
    :type con: sqlite3.Connection
    :returns: Message or error depending whenever database closes or throws error.
    :rtype: str
    """
    try: 
        con.close();
        res = jh.parse_msg("Database Successfully closed!!");
    except sqlite3.Error as err:
        res = jh.parse_error("%s"%(''.join(err.args)));
    return res;

def db_cud(con:sqlite3.Connection, query:string, type:string) -> str:
    """ Does Create Update Delete (CUD without R) operation to specified connection.
    :param con: The database's sqlite3.Connection object to commit a (CUD) operation.
    :type con: sqlite3.Connection
    :param query: (CUD) operation query to be done over the specified database.
    :type param: str
    :param type: Type of (CUD) operation to be conducted.
    :type: str
    :returns: Either a commited message or an error json format as json_dataframe.json
    :rtype: str
    """
    msg = "";
    cur = con.cursor();
    try:
        cur.execute(query);
        con.commit();
        if (type == 'insert'):
            msg = jh.parse_msg(f"{cur.lastrowid}"); # me quedo con el ultimo id insertado.
        else:
            msg = jh.parse_msg("Query successfully commited!!");
    except sqlite3.Error as err:
        msg = jh.parse_error("%s" % (' '.join(err.args)));
    cur.close();
    return msg;
    
def db_select(con:sqlite3.Connection, query:string) -> str:
    """ Does Read (R) operation (select) to specified connection.
    :param con: The database's sqlite3.Connection object to commit a (R) operation.
    :type con: sqlite3.Connection
    :param query: (R) operation to be done over the specified database.
    :type param: str
    :returns: Either a result set in json format or error message (also in json).
    :rtype: str
    """
    cur = con.cursor();
    result_set = None; # as in "result set" (rs)
    try:
        res = cur.execute(query);
        result_set = jh.parse_results(res)
    except sqlite3.Error as err:
        result_set = jh.parse_error("%s" % (' '.join(err.args)))
    cur.close();
    return result_set;

def db_init_table(con:sqlite3.Connection, table_query)-> str:
    """ Initialices table or table triggers
    :param con: connection to database.
    :type: sqlite3.Connection
    :param table_query: query affecting table creation/config.
    :type: Any
    :return: result of operation/error msg.
    :rtype: str
    """
    try:
        cur = con.cursor()
        cur.execute(table_query)
        cur.close()
        answer = jh.parse_msg("Ok! The table has been created/initialized!")
    except sqlite3.Error as err:
        answer = jh.parse_error("%s" % (' '.join(err.args)))
    return answer;