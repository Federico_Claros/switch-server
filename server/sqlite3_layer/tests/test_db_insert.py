import sys
from sqlite3 import Connection

sys.path.append('./../src/');

import Sqlite3Handler as s3h

con = s3h.db_connect("/home/larry/sqlite-db/tutorial.db");

if(type(con) != Connection):
    print(con);
    exit(-1);

msg = s3h.db_cud(con,"insert into movie (title, year, score) values ('Also delete this', 2023, 0.0)");
print(msg);

msg = s3h.db_close(con);
print(msg);