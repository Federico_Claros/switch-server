import json as js

a = """This is an example: "containing 'nested' strings" """
print(a)
a = a.replace("'", "\\\'")
print(a)
a = a.replace('"', "'")

print(a)

a =  "[21, \"Please delete this\", 2023, 0.0]"
print(a)
a = a.replace("\\\"","'")
a = a.replace("\"","'")
print(a)

tupla = tuple((1,'Monty phython and the holy grail', 8.0, 1973))
print(tupla)

j_tupla = js.dumps(tupla)
print(j_tupla)