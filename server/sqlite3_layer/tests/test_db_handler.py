import sys
from sqlite3 import Connection

sys.path.append('./../src/'); # this line is to import my Sqlite3Handler module

import Sqlite3Handler as s3h

path = "/home/larry/sqlite-db/tutorial.db";

connection = s3h.db_connect(path);

if (type(connection) != Connection):
    print(connection);
    exit(-1); # not connection! so close programm

json_result_set = s3h.db_select(connection, "select * from movie");

print(json_result_set);

json_result_set = s3h.db_close(connection);

print(json_result_set)