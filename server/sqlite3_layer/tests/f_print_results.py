import sys
import json as js
from sqlite3 import Connection

sys.path.append("./../../../client/utils/")
import terminaltables as tt

sys.path.append('./../src/'); # this line is to import my Sqlite3Handler module
import Sqlite3Handler as s3h

path = "/home/larry/sqlite-db/tutorial.db";

connection = s3h.db_connect(path);

if (type(connection) != Connection):
    print(connection);
    exit(-1); # not connection! so close programm

json_result_set = s3h.db_select(connection, "select * from movie");

data = js.loads(json_result_set)

print(type(data))
body = data["body"]
columns = body["columns"]
rows = body["rows"]
print(columns)
print(rows)
print(type(rows))
rows.insert(0,columns)

table = tt.SingleTable(rows)
print(table.table)
"""
def unparse_result(json_text:str) -> list:
    parsed_data = js.loads(json_text)
    print (parsed_data)
"""

json_result_set = s3h.db_close(connection);