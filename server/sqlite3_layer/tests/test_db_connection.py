import sys
from sqlite3 import Connection

sys.path.append('./../src/');
import Sqlite3Handler as s3h

"""
    Programa que se conecta a la base de datos sqlite dado nombre bd y un path.
    Para que funcione esto:
        python3 test_db_connection.py <db_name> <db_path>
"""

print("[MSG] initializing: ", sys.argv[0]);

bd_name = "";
path = "";
con = None;

try:
    db_name = sys.argv[1];
except IndexError:
    raise SystemExit("[ERR] Database name not specified!!");

try:
    db_path = sys.argv[2];
except IndexError:
    raise SystemExit("[ERR] Database path is not specified!!");

print("[MSG] specified path is "+db_path+"/"+db_name+".db");

con = s3h.db_connect(db_path+"/"+db_name+".db");

if (type(con) != Connection):
    print("[ERR] database not found!!");
    exit(-1);

rs = s3h.db_close(con);
print(rs)