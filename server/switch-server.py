import socket
import sys
from sqlite3 import Connection
import pg8000

import logger
import primary_db as pdb
import postgres_db as posdb
import query_unwrapper as quw

sys.path.append("./sqlite3_layer/src/")
import JsonHandler as jh

sys.path.append(".postgres_layer/src/")
import PostgresHandler as ph

def _is_error(msg:str)->bool:
    """ Determines if json_server_dataframe is error.
    :param msg: json to be unparsed.
    :type: str
    :returns: True, if it's an error message. False, otherwise.
    :rtype: bool
    """
    msg = jh.unparse(msg);
    if(msg.get("type") == 'error'):
        return True;
    return False;

def _get_sqlite_product(con:Connection, id:int):
    """ Retrieve DB product data from id.
    :param con: Connection to sqlite database.
    :type: Sqlite3.Connection
    :param id: Porduct's id to be retrieved.
    :type: int
    """
    body = {'sql': f'SELECT * FROM product WHERE id = {id}'};
    bkup = jh.unparse(quw.call_sqlite_select(con, body));
    return ((bkup.get('body')).get('rows'))[0];

BUFFER_SIZE = 66560; # hardcoded buffer size to 66560 Bytes or 35 KBytes.

con_log = logger.connect();
con_sqlite_db = pdb.connect();
con_postgres_db = posdb.connect();

if(type(con_log) != Connection or type(con_sqlite_db) != Connection or type(con_postgres_db) != pg8000.Connection):
    print(con_log);
    print(con_sqlite_db);
    print(con_postgres_db)
    exit(-1);

print(logger.init_log_table(con_log));
print(pdb.init_tables(con_sqlite_db));
print(posdb.init_tables(con_postgres_db))

s_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s_sock.bind(('127.0.0.1', 8080))
s_sock.listen(5) # can queue up to 5 requests.

print('[MSG-SWT-SERVER] Listening at {}'.format(s_sock.getsockname()))

while True:
    (c_sock, addr) = s_sock.accept() # accept connection from outside.
    
    addr_str = addr[0] # addr is tuple, so I must get ip addr
    print(logger.log(con_log, addr_str, 'Client has connected to server!')) # log client_socket into sqlite3

    data = c_sock.recv(BUFFER_SIZE);
    data = data.decode("utf-8") # decodes bytes to str in utf-8
    
    obj = jh.unparse(data);

    type = (obj.get('query')).get('type'); # extract client.dataframe.query.type
    body = (obj.get('query')).get('body'); # extract client.dataframe.query.body

    result = jh.parse_msg('This must not be seen by client!');

    if(type == 'insert'): # si es 1 insert
        result = quw.call_sqlite_insert(con_sqlite_db, body);
        if(not _is_error(result)): # si no es error -> continuar la replicacion.
            id = (jh.unparse(result)).get('body')
            # id = result.get('body')

            # llama al postgres para que haga el insert y volve a preguntar si es error:
            rtr = quw.call_postgres_insert(con_postgres_db, body)
            if(not _is_error(rtr)):
                result = jh.parse_msg("Query successfully commited!!");
            else:
                # si es error, rollback_insert_sqlite
                rtr = quw.call_sqlite_delete(con_sqlite_db, {'id': id}) # si queres imprimir rtr.
                result = jh.parse_error('Unable to commit since query error or database not available!');
            
    if (type == 'update'):
        id = body.get('id'); # obtener copia de los datos viejos como bkup por si rollback.
        bkup = _get_sqlite_product(con_sqlite_db, id);
        # extract field data in order to prepare bkup.
        # como se que tengo una tabla (product) voy a descomprimir los datos de bkup aca.
        # de tener mas tablas, tendre que hacer un modulo dedicado.
        id = bkup[0];
        descr = bkup[1];
        stock = bkup[2];
        p_base = bkup[3];
        p_costo = bkup[4];

        result = quw.call_sqlite_update(con_sqlite_db, body);
        if(not _is_error(result)): # continue with replication
            
            # llama al postgres para que haga el update y volve a preguntar si es error:
            rtr = quw.call_postgres_update(con_postgres_db, body)
                
            if(not _is_error(rtr)):
                result = jh.parse_msg("Query successfully commited!!");
            else:
                # si es error, rollback_update_sqlite
                # extraer datos bkup y actualizarlo de nuevo
                query = { 'id': id,'descr': descr, 'stock': stock, 'precio_base': p_base, 'precio_costo': p_costo }
                rtr = quw.call_sqlite_update(con_sqlite_db, query); # si queres imprimir rtr.
                result = jh.parse_error('Unable to commit since query error or database not available!');
        
    if (type == 'delete'):
        id = body.get('id'); # obtener copia de los datos viejos como bkup por si rollback.
        bkup = _get_sqlite_product(con_sqlite_db, id);
        # extract field data in order to prepare bkup.
        # como se que tengo una tabla (product) voy a descomprimir los datos de bkup aca.
        # de tener mas tablas, tendre que hacer un modulo dedicado.
        id = bkup[0];
        descr = bkup[1];
        stock = bkup[2];
        p_base = bkup[3];
        p_costo = bkup[4];
        result = quw.call_sqlite_delete(con_sqlite_db, body);
        if(not _is_error(result)): # continue with replication
            
            # llama al postgres para que haga el delete y volve a preguntar si es error:
            rtr = quw.call_postgres_delete(con_postgres_db, body)
            if(not _is_error(rtr)):
                result = jh.parse_msg("Query successfully commited!!");
            else:
                # si es error, rollback_delete_sqlite
                # extraer datos bkup e insertarlo de nuevo
                query = { 'descr': descr, 'stock': stock, 'precio_base': p_base, 'precio_costo': p_costo }
                rtr = quw.call_sqlite_insert(con_sqlite_db, query,id); # si queres imprimir rtr.
                result = jh.parse_error('Unable to commit since query error or database not available!');
    
    if(type == 'select'):
        """ llamar a postgres para atender el sql select.
        """
        result = quw.call_postgres_select(con_postgres_db,body)
        # result = quw.call_sqlite_select(con_sqlite_db, body)
        '''
            se puede hacer la consulta sobre sqlite tambien si fuera necesario
        '''
    
    c_sock.sendall(bytes(result, encoding="utf-8"));
    c_sock.close();
