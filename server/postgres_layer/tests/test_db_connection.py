import sys
import pg8000

sys.path.append('./../src/')
from PostgresHandler import db_connect_postgres, db_close_postgres
from config import connection_params

"""
    Program that connects to a PostgreSQL database given the database name, user, password, host, and port.
    To run this:
        python3 test_db_connection.py
"""

con = db_connect_postgres(connection_params)

if not isinstance(con, pg8000.Connection):
    print("[ERR] Unable to connect to the database!")
    sys.exit(-1)

# Perform PostgreSQL-specific operations here using 'con'

# Close the PostgreSQL connection
rs = db_close_postgres(con)

print(rs)
