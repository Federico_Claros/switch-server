import sys
import pg8000

sys.path.append('./../src/')  # to import PostgresHandler module
from PostgresHandler import db_connect_postgres, db_close_postgres, db_select_postgres
from config import connection_params

"""
    Program that connects to a PostgreSQL database given the database name, user, password, host, and port.
    To run this:
        python3 test_db_handler.py
"""

con = db_connect_postgres(connection_params)

if not isinstance(con, pg8000.Connection):
    print("[ERR] Unable to connect to the database!")
    sys.exit(-1)

json_result_set = db_select_postgres(con, "select * from product")

print(json_result_set)

json_result_set = db_close_postgres(con)

print(json_result_set)
