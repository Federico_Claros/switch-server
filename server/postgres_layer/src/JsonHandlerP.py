'''
    Interchangable with JsonHandler in all functions except parse_results
    which is specific to pg8000
'''

import json
from pg8000 import Cursor

def parse_msg(message: str) -> str:
    """ Parses message from the database to Json dataframe as specified in json_dataframe.json
    
    :param message: PostgreSQL server's message to be parsed.
    :type message: str
    :returns: Json string with the database's message.
    :rtype: str
    """
    return json.dumps(dict(type="message", body=message))  # Dictionary is serialized into JSON data
    
def parse_results(cur: Cursor) -> str:
    """ Parses executed cursor into a result set from a PostgreSQL database query (R) operation.

    :param cur: Executed cursor to extract information.
    :type: pg8000.Cursor
    :returns: Json string with the database's result set.
    :rtype: str
    """
    l_columns = [desc[0] for desc in cur.description]
    return json.dumps({'type': "result", 'body': {'columns': l_columns, 'rows': cur.fetchall()}}, separators=(',', ':'))
    
def parse_error(err: str) -> str:
    """ Parses the database's error as feedback to be sent.

    :param err: Database error to be parsed.
    :type: str
    :returns: Error parsed as JSON format.
    :rtype: str
    """
    return json.dumps(dict(type="error", body=err))

def unparse_client_query(jsn: str) -> dict:
    """ Parses a JSON string into a Python dictionary.

    :param jsn: JSON string to be parsed.
    :type jsn: str
    :returns: Dictionary parsed from the JSON string.
    :rtype: dict
    """
    return json.loads(jsn)
