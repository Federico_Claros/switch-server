import pg8000
import JsonHandlerP as jh

def db_connect_postgres(connection_params: dict) -> pg8000.Connection:
    """Sets up connection to PostgreSQL database."""
    try:
        connection = pg8000.connect(**connection_params)
        return connection
    except pg8000.Error as e:
        return jh.parse_error(str(e))

def db_close_postgres(con: pg8000.Connection) -> str:
    """Closes PostgreSQL database connection."""
    try:
        con.close()
        return jh.parse_msg("Database successfully closed!!")
    except pg8000.Error as e:
        return jh.parse_error(str(e))

def db_cud_postgres(con: pg8000.Connection, query: str) -> str:
    """Does Create Update Delete (CUD without R) operation on specified connection."""
    msg = ""
    cur = con.cursor()
    try:
        cur.execute(query)
        con.commit()
        msg = jh.parse_msg("Query successfully committed!!")
    except pg8000.Error as e:
        msg = jh.parse_error(str(e))
    cur.close()
    return msg

def db_select_postgres(con: pg8000.Connection, query: str) -> str:
    """Does Read (R) operation (select) on specified connection."""
    cur = con.cursor()
    result_set = None
    try:
        cur.execute(query)
        result_set = jh.parse_results(cur)
    except pg8000.Error as e:
        result_set = jh.parse_error(str(e))
    cur.close()
    return result_set

def db_init_table_postgres(con: pg8000.Connection, table_query: str) -> str:
    try:
        cur = con.cursor()
        cur.execute(table_query)
        cur.close()
        answer = jh.parse_msg("Ok! The table has been created/initialized!")
    except pg8000.Error as e:
        answer = jh.parse_error(str(e))
    return answer
