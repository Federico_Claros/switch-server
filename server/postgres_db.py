import sys
import pg8000

sys.path.append('./postgres_layer/src/')
from PostgresHandler import (
    db_connect_postgres,
    db_close_postgres,
    db_init_table_postgres,
    db_cud_postgres,
    db_select_postgres,
)
import JsonHandlerP as jh
from config import connection_params

def connect() -> pg8000.Connection:
    return db_connect_postgres(connection_params)

def close(con: pg8000.Connection) -> str:
    return db_close_postgres(con)

def init_tables(con: pg8000.Connection):
    create_table_1 = """ CREATE TABLE IF NOT EXISTS product(
                                id SERIAL PRIMARY KEY,
                                descr varchar(100) NOT NULL,
                                stock integer default 0 not null,
                                precio_base double precision not null,
                                precio_costo double precision not null
                        );
                        """
    return db_init_table_postgres(con, create_table_1)

def insert_product(con: pg8000.Connection, descr: str, stock: int, precio_b: float, precio_c: float) -> str:
    query = f"INSERT INTO product (descr, stock, precio_base, precio_costo) VALUES ('{descr}', {stock}, {precio_b}, {precio_c})"
    return db_cud_postgres(con, query)

def reinsert_product(con: pg8000.Connection, id: int, descr: str, stock: int, precio_b: float, precio_c: float) -> str:
    query = f"INSERT INTO product (id, descr, stock, precio_base, precio_costo) VALUES ({id},'{descr}', {stock}, {precio_b}, {precio_c})"
    return db_cud_postgres(con, query)

def delete_product(con: pg8000.Connection, id: int) -> str:
    query = f'DELETE FROM product WHERE id = {id}'
    return db_cud_postgres(con, query)

def update_product(con: pg8000.Connection, id: int, descr, stock, precio_b, precio_c) -> str:
    rtr_str = ''
    query = ''
    if descr is None and stock is None and precio_b is None and precio_c is None:
        rtr_str = jh.parse_error("No se especificó ningún campo a modificar!")
    else:
        placeholders = []
        if descr is not None:
            placeholders.append(f"descr = '{descr}'")
        if stock is not None:
            placeholders.append(f'stock = {stock}')
        if precio_b is not None:
            placeholders.append(f'precio_base = {precio_b}')
        if precio_c is not None:
            placeholders.append(f'precio_costo = {precio_c}')
        query = f'UPDATE product SET {", ".join(placeholders)} WHERE id = {id}'
        rtr_str = db_cud_postgres(con, query)
    return rtr_str

def select(con: pg8000.Connection, query) -> str:
    # Here should be query sanitization.
    return db_select_postgres(con, query)
