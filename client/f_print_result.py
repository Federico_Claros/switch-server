import sys
import json

sys.path.append("./utils/")

import terminaltables as tt

""" The module will print the result into a table.
    :version: 1.0
"""

def show_result(result:str)-> None:
    """ Receives json object, unparses and prints result into table.
    :param result: result from database to be unparsed and printed.
    :type: str
    """
    j = json.loads(result);
    t = j.get("type");
    body = j.get("body");
    
    if (t != 'result'):
        print("["+t+"] "+ body);
    else:
        columns = body["columns"]
        rows = body["rows"]
        
        rows.insert(0,columns)
        
        table = tt.SingleTable(rows);
        table.inner_heading_row_border = True;
        table.inner_heading_column_boder = True;
        table.title = t; 
        print(table.table)