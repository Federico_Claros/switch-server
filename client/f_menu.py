import sys

sys.path.append("./utils/")

import terminaltables as tt

""" Set of functions that handle main menu printing.
    :version: 1.0
"""

def show_menu(table:tt.SingleTable)-> int:
    """ Show menu on console.
    :param: table to be printed.
    :type: SingleTable.
    :returns: Selected option.
    :type: int
    """
    print(table.table);
    opcion = int(input('Ingresar opción: '))
    return opcion

def init_menu() -> tt.SingleTable:
    """ Initializes a menu of options.
    :returns: A table with the menu ready to be used.
    :rtype: SingleTable
    """
    data = [];
    data.append(['1 - Registrar producto.']);
    data.append(['2 - Borrar un producto.']);
    data.append(['3 - Modificar producto.']);
    data.append(['4 - Consultar producto.']);
    data.append(['0 - Terminar programa.'])

    table = tt.SingleTable(data);
    table.inner_heading_row_border = False;
    table.title = 'Menu de Opciones';
    return table;