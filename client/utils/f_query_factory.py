import JsonParser as jp

""" This functions will provide app. level queries formated in json to be send to server.
    :version: 1.0
"""

SELECT_DEFAULT = "SELECT * FROM PRODUCTO";

def parse_select_prod(sql:str=SELECT_DEFAULT)->str:
    """ Parses sql query into json ready to be sent to server.
    :param: sql query.
    :type: str
    :returns: json ready to be sent.
    :rtype: str
    """
    query = {'table': "product", 'query': {
        'type': "select", 
        'body': { 'sql': sql }
        }
    }
    return jp.parse_query(query);

def parse_insert_product(descr, stock, precio_base, precio_costo)->str:
    """ Parses insert data into json ready to be sent to server.
    :param: product description.
    :type: Any
    :param: product stock.
    :type: Any
    :param: product base_price.
    :type: Any
    :param: product cost_price.
    :type: Any
    :return: json ready to be sent.
    :rtype: str
    """
    query = {'table': "product", 'query': {
        'type': "insert", 
        'body': { 
            'descr': descr, 'stock': stock, 'precio_base': precio_base, 'precio_costo': precio_costo
            } 
        } 
    }
    return jp.parse_query(query);

def parse_delete_product(id:int)->str:
    """ Parses delete data into json, ready to be sent to server.
    :param: product id to be deleted.
    :type: int
    :return: json ready to be sent.
    :rtype: str
    """
    query = {'table': "product", 'query':{
        'type': "delete",
        'body': { 'id': id }
    }}
    return jp.parse_query(query);

def parse_update_product(id:int, descr, stock, precio_base, precio_costo)-> str:
    """ Parses update data into json ready to be sent to server.
    :param: product id.
    :type: int
    :param: new product description.
    :type: Any
    :param: new product stock.
    :type: Any
    :param: new product base_price.
    :type: Any
    :param: new product cost_price.
    :type: Any
    """
    query = {'table': "product", 'query': {
        'type': "update", 
        'body': { 
            'id': id,'descr': descr, 'stock': stock, 'precio_base': precio_base, 'precio_costo': precio_costo
            } 
        } 
    }
    return jp.parse_query(query);