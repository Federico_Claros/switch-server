import utils.f_query_factory as qf

""" this module will ask for user input, and parse it into json.
:version: 1.0
"""

def insert_product()->str:
    """ Init dialog with user so as to get data for insert product.
    :returns: json for the insert.
    :rtype: str
    """
    query = '0';
    descr = input("Ingresar descr: ");
    stock = input("Ingresar Stock: ");
    precio_base = float(input("Ingresar precio base: "));
    precio_costo = float(input("Ingresar precio costo: "));
    print('Confirmar {'+ f"{descr}"+', '+ f"{stock}"+', '+f"{precio_base}"+', '+f"{precio_costo}"+'} ?');
    confirmar = input("Ingrese Y o N: ");
    if (confirmar == 'y' or confirmar == 'Y'):
        query = qf.parse_insert_product(descr, stock, precio_base, precio_costo);
    return query;

def delete_product()->str:
    """ Init dialog with user so as to get data for delete product.
    :returns: json for the insert.
    :rtype: str
    """
    query = '0';
    
    id = int(input("Ingresar ID del producto: "));
    
    print(f"Confirmar id = {id} ?");
    confirmar = input("Ingrese Y o N: ");
    
    if (confirmar == 'y' or confirmar == 'Y'):
        query = qf.parse_delete_product(id);
    
    return query;

def update_product()->str:
    """ Init dialog with user so as to get data for update product.
    :returns: json for the insert.
    :rtype: str
    """
    query = '0';
    id = int(input("Ingresar ID del producto a mod: "));
    
    print(f"Confirmar id = {id} ?");
    confirmar = input("Ingrese Y o N: ");
    
    if (confirmar == 'y' or confirmar == 'Y'):
        descr = input("Ingresar descr (Enter para omitir): ");
        
        if descr == '':
            descr = None;
        
        stock = input("Ingresar Stock (Enter para omitir): ");
        
        if stock != '':
            stock = stock;
        else: 
            stock = None;

        precio_base = input("Ingresar precio base (Enter para omitir): ");
        
        if precio_base != '':
            precio_base = float(precio_base);
        else: 
            precio_base = None;
        
        precio_costo = float(input("Ingresar precio costo (Enter para omitir): "));
        
        if precio_costo != '':
            precio_costo = float(precio_costo);
        else: 
            precio_costo = None;

        print('Confirmar {'+f"{id}"+', '+ f"{descr}"+', '+ f"{stock}"+', '+f"{precio_base}"+', '+f"{precio_costo}"+'} ?');
        
        confirmar = input("Ingrese Y o N: ");
        if (confirmar == 'y' or confirmar == 'Y'):
            query = qf.parse_update_product(id, descr, stock, precio_base, precio_costo);
    return query;

def select_product()->str:
    """ Init dialog with user so as to make select query.
    :returns: json for the insert.
    :rtype: str
    """
    print("Ingresar consulta en sql: ")
    query = input();
    return qf.parse_select_prod(query);