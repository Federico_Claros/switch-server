import json

""" A module that parse (poorly) json in order to be send through internet.
    :version: 1.0
"""

def parse_msg(message:str) -> str:
    """ Parses message from database to Json dataframe as specified in json_dataframe.json
    :param message: Sqlite3 server's message to be parsed.
    :type message: str
    :returns: Json_string with database's message.
    :rtype: str
    """
    return json.dumps(dict(type= "message", body=message)); # dictionary is serialize into json_data
    
def parse_error(err:str) -> str:
    """ Parses database's error as feedback to be send.
    :param err: Database error to be parsed.
    :type: str
    :returns: Error parsed as like json_dataframe.json format.
    :rtype: str
    """
    return json.dumps(dict(type="error", body=err));

def parse_query(dictionary:dict)->str:
    """ Parses query into a json.
    :param: dictionary to be parsed.
    :type: dict
    :returns: query parsed into json.
    :rtype: str
    """
    return json.dumps(dictionary, separators=(',',':'));