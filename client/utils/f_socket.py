import socket
import JsonParser as jp

""" Set of functions that handles sockets and connections.
    :version: 1.0
"""

BUFFER_SIZE = 66560; # hardcoded buffer size to 66560 Bytes or 35 KBytes.

SERVER_ADDR = '127.0.0.1'; # default server address.
SERVER_PORT = 8080; # default server port.

def init_socket()->socket:
    return socket.socket(socket.AF_INET, socket.SOCK_STREAM); # make AF_INET, TCP socket.

def recvall(sock:socket.socket)-> bytearray:
    # stub function, originally intended for receive a lot of data, now does simple recv()
    data = bytearray();
    """
    bytes_recd = 0;
    
    while bytes_recd < BUFFER_SIZE :
        chunk = sock.recv(min(BUFFER_SIZE - bytes_recd , BUFFER_SIZE)); # read data from socket buffer.

        if chunk == b'':
            print('Socket connection broken!!')
            bytes_recd = BUFFER_SIZE; # the socket connection was broken, finnish loop
        
        data.extend(chunk);
        bytes_recd = bytes_recd + len(chunk);
    """
    data.extend(sock.recv(BUFFER_SIZE));
    return data;

def send_data(sock:socket.socket, data:bytes, addr:str=SERVER_ADDR, port:int=SERVER_PORT)->str:
    """ Use socket to send data to server, returns string
    :param socket: socket to be used in connection.
    :type socket: socket.socket
    :param data: Application layer data to be sent.
    :type: bytes.
    :param addr: Server address to connect. Default 171.0.0.1
    :type: str
    :param port: Server port to connect. Default 8080
    :type: int
    :returns: Json or str with information/error/results.
    :rtype: str
    """
    rtr_data = ''; 
    try:
        sock.connect((addr, port));
        sock.sendall(data);

        rtr_data = recvall(sock).decode("utf-8"); # transform received data (bytearray) into readable data.

        sock.close(); # after sending and receiving data, close connection.
    except socket.error as err:
        rtr_data = jp.parse_error(str(err)); # make error json data in order to inform app of error.
    return rtr_data; # return data to client application.
