import sys

sys.path.append("./utils/")

import terminaltables as tt

table_data = [
    ['Heading1', 'Heading2'],
    ['row1 column1', 'row1 column2'],
    ['row2 column1', 'row2 column2'],
    ['row3 column1', 'row3 column2']
]
table = tt.AsciiTable(table_data)
print(table.table)

table = tt.SingleTable(table_data)
print(table.table)