import f_menu as menu
import utils.f_dataentry_product as dp
import utils.f_socket as fst
import f_print_result as fpr

def option_query_selector(option:int)->str:
    """ Based on the integer it parses a query ready to be sent to server.
    :param option: option that will make an specific sql query.
    :type: int
    :returns: Json ready to be sent to server.
    :rtype: str
    """
    query = '0';
    if (option == 1): # insert 1 producto
        query = dp.insert_product(); # llamar dialogo con usuario para pedir datos del producto.
    if (option == 2):
        query = dp.delete_product(); # llamar dialogo con usuario para pedir id del producto a eliminar.
    if (option == 3):
        query = dp.update_product() # llamar dialogo con usuario para pedir id y datos del producto a mod.
    if (option == 4):
        query = dp.select_product(); # llamar dialogo con usuario para pedir id  o mostrar todos los productos.
    return query;


m = menu.init_menu();
option = menu.show_menu(m);

while (option != 0):
    sock = fst.init_socket(); # for each query, I must create a new socket.
    query = option_query_selector(option);
    answer = fst.send_data(sock,query.encode());
    
    fpr.show_result(answer)
    
    option = menu.show_menu(m);
    